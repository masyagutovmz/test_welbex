output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.vm.public_ip
}

output "elb_dns_name" {
  description = "The DNS name of the ELB"
  value       = aws_elb.my-elb.dns_name
}