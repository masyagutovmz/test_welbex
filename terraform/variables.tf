variable "instance_name" {
    default = "my_nginx"
}
variable "AWS_REGION" {
    type = string
}
variable "AWS_SECRET_ACCESS_KEY" {
    type = string
}
variable "AWS_ACCESS_KEY_ID" {
    type = string
}